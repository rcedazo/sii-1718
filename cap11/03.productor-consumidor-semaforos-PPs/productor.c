// Compilar con opción -lpthread
// gcc productor.c -lpthread -o productor
// ./productor
// Ver los semáforos y el BUFFER en el directorio /dev/shm/

#include <sys/mman.h>
#include <stdio.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>

#define MAX_BUFFER 1024
#define DATOS_A_PRODUCIR 100000

sem_t *huecos; 		/* tamaño del buffer */
sem_t *elementos;	/* datos a producir */

int *buffer; 		/* puntero al buffer de números enteros */

void productor(void);

int main(void){

	int fd; // Descriptor de fichero

	/* se crean e inician semáforos */
	huecos = sem_open("HUECOS", O_CREAT, 0700, MAX_BUFFER);
	elementos = sem_open("ELEMENTOS", O_CREAT, 0700, 0);
	if (huecos == SEM_FAILED || elementos == SEM_FAILED) {
	  perror("Error en sem_open");
	  return 1;
	}

	/* se crea el segmento de memoria compartida utilizado como buffer circular */
	fd = open("/tmp/buffer", O_CREAT|O_RDWR, 0700);
	if (fd < 0) {
		perror("Error en open");
		return 1;
	}
	// Se le da el tamaño maximo del buffer
	ftruncate(fd, MAX_BUFFER*sizeof(int));
	buffer = (int *)mmap(NULL, MAX_BUFFER*sizeof(int), PROT_WRITE, MAP_SHARED, fd, 0);
	if (buffer == NULL) {
	  perror("Error en mmap");
	  return 1;
	}

	productor();		/* se ejecuta el código del productor */

	/* se desproyecta el buffer */
	munmap(buffer, MAX_BUFFER*sizeof(int));
	close(fd);

	/* cierran y se destruyen los semáforos */
	sem_close(huecos);
	sem_close(elementos);
	sem_unlink("HUECOS");
	sem_unlink("ELEMENTOS");
	return 0;
}

/* código del proceso productor */
void productor(void){
	int dato; 		/* dato a producir */
	int posicion = 0;	/* posición donde insertar el elemento*/
	int j;

	for (j=0; j<DATOS_A_PRODUCIR; j++) {
	  dato = j;
	  sem_wait(huecos);			/* un hueco menos */
	  buffer[posicion]=dato;
	  printf ("Dato producido = %d\n", dato);
	  posicion=(posicion+1) % MAX_BUFFER;	/* nueva posición */
	  sem_post(elementos);			/* un elemento más */
	}
	
	return;
}


