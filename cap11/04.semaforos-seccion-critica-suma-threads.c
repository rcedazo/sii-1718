// Compilar con opcion: -lpthread

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <semaphore.h>

#define NUM_THREADS 2

int suma_total = 0;
int fd[2];
sem_t semaforo;

void * sumar(void *n) {
	int inicio, fin, sumParcial=0, i;
	pid_t tid;
	int dato;
	
	inicio = (int)n * 10 + 1;
	fin = inicio + 9;

	for (i=inicio; i<=fin; i++) {
		sumParcial+=i;
	}

	printf ("Thread suma parcial [%d-%d] = %d\n", inicio, fin, sumParcial);
	sem_wait(&semaforo);
	suma_total += sumParcial;
	sem_post(&semaforo);
	pthread_exit(0);
}

int main() {
	pthread_attr_t attr;
	pthread_t thid[NUM_THREADS];
	int i;
	// Inicializo el semaforo a 1
	sem_init(&semaforo, 0, 1);

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	
	for(i=0; i<NUM_THREADS; i++) {	
		pthread_create(&thid[i], &attr, sumar, (void *)i);
	}
	
	/* Wait on the other threads */
	for(i=0;i<NUM_THREADS;i++) {
  		pthread_join(thid[i], NULL);
  	}
	
	printf("Resultado = %d\n", suma_total);

	sem_destroy(&semaforo);

	return 0;
}
