// Programa en C que calcula la raiz cuadrada de un numero pasado como argumento
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main (int argc, char *argv[])
{
  if (argc < 2) {
    fprintf(stdout,"Uso: %s numero\n",argv[0]);
    return 1;
  }
  double inputValue = atof(argv[1]);
  double outputValue = sqrt(inputValue);
  fprintf(stdout,"La raiz cuadrada de %g es %g.\n", inputValue, outputValue);
  return 0;
}
